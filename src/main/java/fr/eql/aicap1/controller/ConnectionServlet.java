package fr.eql.aicap1.controller;

import fr.eql.aicap1.jdbc.demo.dao.CatDao;
import fr.eql.aicap1.jdbc.demo.dao.OwnerDao;
import fr.eql.aicap1.jdbc.demo.entity.Cat;
import fr.eql.aicap1.jdbc.demo.entity.Owner;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/cServlet")
public class ConnectionServlet extends HttpServlet {

    private final OwnerDao ownerDao = new OwnerDao();
    private final CatDao catDao = new CatDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String login = req.getParameter("loginParam");
        String password = req.getParameter("passwordParam");
        Owner owner = ownerDao.authenticate(login, password);

        if (owner != null) {
            List<Cat> cats = catDao.findByOwner(owner);
            req.setAttribute("catsParam", cats);
            HttpSession session = req.getSession();
            session.setAttribute("ownerParam", owner);
            req.getRequestDispatcher("DisplayOwnerCats.jsp").forward(req, resp);
        } else {
            req.setAttribute("messageParam", "Identifiant et/ou mot de passe incorrect(s).");
            req.getRequestDispatcher("Connection.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
