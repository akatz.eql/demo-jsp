<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Mes chats</title>
</head>
<body style="background-color: lightblue">
<div style="border: solid black 1px; background-color: teal">
        <span style="float: right; margin-right: 1vh; font-size: 3vh">
            Bonjour ${ownerParam.name}
            <form method="post" action="dServlet">
                <input type="submit" value="Déconnexion">
            </form>
        </span>
    <h1 style="text-align: center">Mes chats</h1>
</div>
<c:forEach items="${catsParam}" var="cat">
    <fieldset style="float: left; height: 35vh; margin: 2vh; padding: 2vh; display: flex">
        <legend style="font-size: 3vh">${cat.name}</legend>
        <img style="height: 30vh" src="${cat.picture}">
        <ul style="margin-top: 5vh">
            <li>Race : ${cat.breed}</li>
            <li>Date de naissance : ${cat.birthdate}</li>
        </ul>
    </fieldset>
</c:forEach>
</body>
</html>