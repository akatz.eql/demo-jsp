<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Connexion</title>
</head>
<body>
    <h1>Connexion</h1>
    <form action="cServlet" method="post">
        <label>Identifiant</label>
        <input type="text" name="loginParam"><br />
        <label>Mot de passe</label>
        <input type="password" name="passwordParam"><br />
        <input type="submit" value="Connexion !">
    </form>
    <c:if test="${messageParam != null}">
        ${messageParam}
    </c:if>
</body>
</html>